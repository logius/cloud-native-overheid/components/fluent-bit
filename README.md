# fluent-bit

[Fluent Bit](https://fluentbit.io/) is used for log forwarding.

Links:
- Chart sources and flags: https://github.com/fluent/helm-charts/tree/master/charts/fluent-bit
- Releases: https://fluentbit.io/announcements/
- Docs: https://docs.fluentbit.io/manual/

## Workings
Fluent Bit runs as a daemon set on each node. It is configured to use the container logs on the node as input, apply several filters and send the output to Elastic.

Set the correct container runtime in `config.fluent.input_parser` (docker, cri, ...) so Fluent Bit can interpret the logs.

Notable filters:
* Kubernetes: based on the name of the logfile, which contains namespace, pod id and container name, retrieve Kubernetes meta data
  * Labels included if `config.fluent.include_resource_labels` is true, annotations included if `config.fluent.include_resource_annotations` is true
* json parser: if the log message is in JSON format, parse it and create a key-value pair for each attribute
  * Only activated if `config.fluent.parse_json` is true
  * In case you want to overwrite existing keys in the message with values parsed from the JSON, set `config.fluent.preserve_key` to true.

See https://docs.fluentbit.io/manual/concepts/data-pipeline for more details.

## Features:
### config.fluent.input_parser support for docker engine cri-o

When other container engine is used instead of docker like CRI-O or Containerd another parser can be selected.

To use this parser change the Input section for your configuration from `docker` to `cri`
https://docs.fluentbit.io/manual/installation/kubernetes#container-runtime-interface-cri-parser


```yaml
config:
  fluent:
    input_parser: cri
```

### config.fluent.use_deprecated_multiline_parser
Since 1.8 Fluent Bit has a different way of parsing multi line log messages, see https://docs.fluentbit.io/manual/pipeline/inputs/tail#multiline-support

The old style can still be used but is deprecated in Fluent Bit and also in CNO.
