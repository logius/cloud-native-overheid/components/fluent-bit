function set_index(tag, timestamp, record)
    if record["kubernetes"] ~= nil and record["kubernetes"]["namespace_name"] ~= nil then
        if isempty(record["platform.prefix"]) then
            record["es_index"] = record["kubernetes"]["namespace_name"]
				else
            record["es_index"] = record["platform.prefix"] .. record["kubernetes"]["namespace_name"]
				end
    end
    return 1, timestamp, record
end

function isempty(s)
  return s == nil or s == ''
end

function parse_keycloak_event(tag, timestamp, record)
	if record["loggerName"] == "org.keycloak.events" then
			result = Split(record["message"], ",")
			record['keycloak'] = {}
			for k,v in ipairs(result) do
					entry = Split(v, "=")
					record['keycloak'][trim(entry[1])] = trim(entry[2])
			end
			return 1, timestamp, record
	end
  return 0, timestamp, record
end

function Split(s, delimiter)
	result = {};
	for match in (s..delimiter):gmatch("(.-)"..delimiter) do
			table.insert(result, match);
	end
	return result;
end

function trim(s)
  return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end
